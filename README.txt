Lancement de l'application :
 - étape 1 : lancer les services de gestion (consul, rabbitMQ, bases de données) 
==> docker-compose up -d
 - étape 2 : lancer les microservices Java (gateway, utilisateur, cours, springadmin)
(Cette étape requiert des ressources Docker importantes, 2GB n'est pas suffisant, 4GB est possible)
==> mvn clean install -DskipTests sur chaque service (utilisateur-service, cours-service, oauth-service, spring-boot-admin, gateway-service)
==> docker-compose -f docker-compose.microservices.yml build
==> docker-compose -f docker-compose.microservices.yml up -d
 - étape 3 : lancer les microservices (utilisateur et cours) en doublon (pour failover et load balancing) ==> changer les hostnames du Coker-compose et relancer des instances (nécessité de 6GB et 6CPU pour docker)
 - étape 4 : utiliser l'api
 - étape 5 : fermer tous les docker
==> docker-compose -f docker-compose.microservices.yml stop
==> docker-compose -f docker-compose.microservices.yml rm -fv
==> docker-compose stop
==> docker-compose rm -fv

Authorization:
 - type : OAuth 2.0
 - Grant Type : Password Credentials
 - Access Token URL : http://localhost:9191/oauth/token
 - Client ID : api-client
 - Client Secret : password
 - Username : operrin / qdesmet
 - Password : password
 - Scope : read, write

Routes disponibles par swagger:
localhost:9000/swagger-ui.html
